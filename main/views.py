from django.shortcuts import render
from django.http import HttpResponse

from .models import Chase

def index(request):
    chase_running = Chase.objects.get(pk=1).session_running
    return render(request, 'main/index.html', {'chase_running': chase_running})