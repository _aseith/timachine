from django.db import models
from django_fsm import FSMIntegerField, transition
from solo.models import SingletonModel
from datetime import datetime, timedelta
from .tasks import *

class Chase(SingletonModel):
	# Definition of states
	STATE_NEW 			= 0
	STATE_FIRSTHOUR		= 1
	STATE_SECONDHOUR	= 2
	STATE_THIRDHOUR		= 3
	STATE_SOLVED		= 4
	STATE_TIMEOUT		= 5
	STATE_ERROR			= 6
	STATE_CHOICES = (
		(STATE_NEW, 'new'),
		(STATE_FIRSTHOUR, 'first_hour'),
		(STATE_SECONDHOUR, 'second_hour'),
		(STATE_THIRDHOUR, 'third_hour'),
		(STATE_SOLVED, 'solved'),
		(STATE_TIMEOUT, 'timeout'),
	)

	state = FSMIntegerField(
		choices=STATE_CHOICES,
		default=STATE_NEW, 
		protected=False,
	)

	# Caller number
	caller_id = models.CharField(
		max_length=20,
		default='0',
	)
	# How many times have we been called?
	caller_counter = models.PositiveIntegerField(default=0)

	maintenance_mode = models.BooleanField(default=False)
	session_running = models.BooleanField(default=False)

	SERVICE_CELLULAR = 'CL'
	SERVICE_TELEGRAM = 'TG'
	SERVICE_FACEBOOK = 'FB'
	#SERVICE_WHATSAPP = 'WA'

	TYPE_OF_SERVICE = (
		(SERVICE_CELLULAR, 'Phone'),
		(SERVICE_TELEGRAM, 'Telegram'),
		(SERVICE_FACEBOOK, 'Facebook Messenger'),
	)

	type_of_service = models.CharField(
		max_length=2,
		choices=TYPE_OF_SERVICE,
		default=SERVICE_CELLULAR,
	)

	@transition(field=state, source='*', target=STATE_NEW)
	def reset(self):
		caller_id = '0'
		caller_counter = 0
		session_running = False

	def cleanup(self):
		self.reset()
		self.save()

	@transition(field=state, source=STATE_NEW, target=STATE_FIRSTHOUR)
	def start_first_hour(self):
		session_running = True
		proceed_to_second_hour.apply_async((),eta=datetime.utcnow() + timedelta(minutes=1))
		print('First hour started')

	@transition(field=state, source=STATE_FIRSTHOUR, target=STATE_SECONDHOUR)
	def start_second_hour(self):
		proceed_to_third_hour.apply_async((),eta=datetime.utcnow() + timedelta(minutes=1))
		print('Second hour started')

	@transition(field=state, source=STATE_SECONDHOUR, target=STATE_THIRDHOUR)
	def start_third_hour(self):
		proceed_to_timeout.apply_async((),eta=datetime.utcnow() + timedelta(minutes=1))
		print('Third hour started')

	@transition(field=state, source=[STATE_FIRSTHOUR, STATE_SECONDHOUR, STATE_THIRDHOUR], target=STATE_SOLVED)
	def solve(self):
		# some stuff to be done before cleanup
		self.cleanup()
		print('Cache solved')

	@transition(field=state, source=STATE_THIRDHOUR, target=STATE_TIMEOUT)
	def timeout(self):
		# some stuff to be done before cleanup
		self.cleanup()
		print('Time is out, we killed your sister')

	def start_chase(self):
		self.start_first_hour()
		self.save()
		print('Chase started')