from __future__ import unicode_literals
from celery import shared_task

@shared_task
def proceed_to_second_hour():
	# needed here to prevent import loop
	from .models import Chase
	c = Chase.objects.get(pk=1)
	c.start_second_hour()
	c.save()

@shared_task
def proceed_to_third_hour():
	from .models import Chase
	c = Chase.objects.get(pk=1)
	c.start_third_hour()
	c.save()

@shared_task
def proceed_to_timeout():
	from .models import Chase
	c = Chase.objects.get(pk=1)
	c.timeout()
	c.save()