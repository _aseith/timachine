from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import Chase

admin.site.register(Chase, SingletonModelAdmin)