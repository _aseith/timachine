# Docker based Timachine

Uses Docker Compose, Python 3, Django 1.11, Celery 4.1, Rabbitmq 3.7, PostgreSQL 10.1.

## Setup

### Development

If you start everything for the first time, you first need to run all database migrations

    docker-compose run web python manage.py migrate

Then, start everything in attached mode to see what's happening

    docker-compose up

Or, if you prefer to have it running in background

	docker-compose up -d

Now, we need to create the "one and only" chase. It's a singleton because only one chase can run at a given time. For that, invoke the Django shell like so

	docker-compose run web python manage.py shell

In the shell, create the chase:

	from main.models import Chase
    c = Chase()
    c.save()

To then start the chase and test everything:

     c.start_chase()

#### Stopping

If running in attached mode, simply use Ctrl-C to stop everything, otherwise

	docker-compose stop

#### Removing everything

Is as simple as

    docker-compose down

This deletes all containers and all data, which means you need to rerun setup from above!

#### Running migrations

If you updated the model and added/removed/modified fields, run:

	docker-compose run web python manage.py makemigrations
    docker-compose run web python manage.py migrate

### Production

TBD

## References

Created by following this tutorial:

https://docs.docker.com/compose/django/#create-a-django-project
