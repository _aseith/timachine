FROM python:3.6
LABEL maintainer="team@dosenmatrosen.com"

ENV PYTHONUNBUFFERED 1
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
ADD . /usr/src/app/
